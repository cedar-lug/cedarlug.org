# cedarlug.org

This is the repository where development of the [cedarlug website](https://cedarlug.org) takes place. 

## What is Cedar LUG? 

The Cedar LUG is a place for discussions related to Linux and Linux related topics. This groups is a club representing the University of Northern Iowa. However, meetings are open to any community members wishing to participate. 

## How do I Contribute?

You can find information about setting up an environment to contribute to this site at our [contributing page](https://gitlab.com/cedar-lug/cedarlug.org/blob/master/CONTRIBUTING.md)