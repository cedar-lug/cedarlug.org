# What Skill Level Do You Need to Contribute?

This repository is designed as an entry point to open source contribution. That means that the skill required to contribute is none. 
If you are interested in making a change to the site you're more than free to either independently submit pull requests. If you're new to git you can get in contact 
with any [Cedar LUG officer](https://cedarlug.org/officers) and they can offer any help you need to get started. 

Along with offering help with git this platform would be a good opportunity to learn any of the technologies our site uses (e.g Flask,HTML,MaterialieCSS). 

# Setting Up Your Environment

These instructions assume you're using a UNIX based system with *pip*. If you need additional help please feel free to get in contact with a [Cedar LUG officer](https://cedarlug.org/officers)
and any of them would be more than happy to provide additional instructions.

In my own experience the best way to work with flask is to use a pip virtual environment so your personal computers pip requirements and this projects are seperate. 
For more reading on this topic you can visit the [python.org explanation of pip and virtualenv](https://packaging.python.org/guides/installing-using-pip-and-virtualenv/)

### Cloning This Repository

On your local machine you can enter the terminal and ```cd``` into the directory you would like the "cedarlug.org" directory to reside in. 

Once you have put your terminal in this location you can simply use the command ```git clone https://gitlab.com/cedar-lug/cedarlug.org``` which will download 
this repository into a directory called "cedarlug.org".

### Setting up the Virtual Environment

If pip is already on your machine insatlling virtualenv (virtual environment) for pip should be as simple as running ```pip install virtualenv --user```

After changing your active directory to be inside of the "cedarlug.org" directory you can use the command ```pip virtualenv env``` where 
env is the name of the directory that will be created. env is included in the .gitignore file already so using that directory name will *1)* make the following instructions
easier to use and *2)* ensure that you don't accidentally commit your virtual environment directory to the repository

Now that we have a virtual environment where we are seperate from the normal system we need to "activate" it which means we need ot tell the terminal that we would like to
be using that virtual environment instead of our normal environment. To do this (while in the cedarlug.org directory) we can run the command ```source env/bin/activate```.

After running this command you should notice your terminal looks slightly different:

```
[user@localhost cedarlug.org]$ source env/bin/activate
(env) [user@localhost cedarlug.org]$ 
```

This (env) is how we know we were successfull. You can exit back to your normal shell at any time using the command ```deactivate```

```
(env) [user@localhost cedarlug.org]$ deactivate 
[user@localhost cedarlug.org]$ 
```

While using this virtual environment you can at any time check what is installed through pip by using the command ```pip freeze```. When you first create the 
virtual environment and start using it this should return nothing. 

### Installing the Requirements for Flask Using Virutalenv

To install everything we need for this project we will be using the file in the cedarlug.org directory. This is as simple as running the command

```pip install -r requirements.txt```

This should take care of all the installations for you. With that run you will be completely setup and ready to develop. 

### Uninstalling Excess pip Modules

If you are testing a pip module and decide to uninstall everything later you can go either two routes. 

1. Use the command ```pip freeze | grep -v -f requirements.txt - | grep -v '^#' | xargs pip uninstall -y``` which uninstalls anything not in the requirements.txt file
2. Delete your env folder and create a new virtual environment from scratch.
3. 

# Understanding the Flask Files

If you would like a more in-depth/example based explanation of flask I can recommend [This youtube tutorial](https://www.youtube.com/watch?v=zRwy8gtgJ1A) which I found
useful when learning thi technology myself. 

### app.py

This is the entry point for all of the traffic to the website and this acts as the brains for the operation. All routing is done in this file. 
A typical route looks something like so:

```
@app.route
def foo('/foo'):
    return render_template('bar.html')
```

To simply break this down there are three main components.

1. @app.route which needs to be above every route function
2. The function itself. While the name of the function doesn't matter the parameter needs to be the string that goes at the end of the url in the browser. For example, in the above code we would access it by going to https://cedarlug.org/foo"
3. the return statement is a flask function called "render_template" which takes as input the name of an html file. This file needs to be located in the directory /templates 

### static/

The static folder is a place where all static files like images, js, and css can be stored and served to users.

### templates/

The templates folder is where we put all of the html files that app.py will be rendering to the screen for our users. 

### templates/includes

templates are capable of using any html file in the includes folder as a way to avoid rewriting html on every page and centralizing those sections.
For example instead of putting the HTML for the navbar on every single html file we can instead put it in ```templates/includes/_navbar.html``` which we can then
access in our ```templates/index.html``` by adding the line ```{% include 'includes/_navbar.html' %}``` at the top of the body tag. 

# Running Flask

Now that you've setup your environment you're ready to start running the webserver and making changes to files. The first step is to enable "debugging mode" for flask. 
We can do this by from the terminal using the command ```export FLASK_ENV=development```. Then to start the actual program we can use the command ```flask run```.

The output should look like such:

```
(env) [user@localhost cedarlug.org]$ export FLASK_ENV=development
(env) [user@localhost cedarlug.org]$ flask run
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 775-888-099
```

from this terminal window the program will continue to run infinitely until you stop it with a keyboard interrupt (CTRL + C). 

It will also log for you all requests into the server including:

- What IP it originated from
- The date requested
- The type of request
- The file requested
- The HTTP return code

```
127.0.0.1 - - [21/Dec/2018 12:26:13] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [21/Dec/2018 12:26:13] "GET /static/tux.png HTTP/1.1" 304 -
127.0.0.1 - - [21/Dec/2018 12:26:13] "GET /favicon.ico HTTP/1.1" 404 -
```

The reason we use debugging mode is because that means changes to files are updated on the website in real time once you save your files.

# Materialize CSS

The CSS framework we are currently using for this website is Materialize. Documentation about how to use it can be found on [their website](https://materializecss.com)

A refrence to the cdn for this css and js are built into the layout.html file that is extended for every page so you should not have to create links on each page for them. 