#!/usr/bin/python
import sys

activate_this = '/var/www/cedarlug.org/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(0,"/var/www/cedarlug.org")
from app import app as application
