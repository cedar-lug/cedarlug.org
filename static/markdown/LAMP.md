# What is LAMP?

LAMP is an acronym that stands for Linux, Apache, MySQL, PHP. While you can use any alternatives to these technologies and get the same effect this is the combination that has been popularized and is in my opinion the most beginner friendly. 

The product of installing all of these technologies onto our system is that we get all of the possible functionality possible out of a web server. We have [Apache](https://en.wikipedia.org/wiki/Apache_HTTP_Server) to host our website in a way browsers can view it, [MySQL](https://en.wikipedia.org/wiki/MySQL) to act as our database if we are storing any information in the long term, and [PHP](https://en.wikipedia.org/wiki/PHP) for all of our server side code needs (such as interacting with the MySQL database). and Linux to bring the whole thing together to one system. 

# Why Run a Web Server?

There are lots of reasons you could want to have your presence on the internet. There are many advertisting and business reasons but those aren't of interest to me. From the perspective of a college student my website has served me two primary purposes. 

**1)** A portfolio that I can present to employers while job hunting. At the time of presentation I have already finished job hunting but I can say from experience that having your own website on your resume shows a lot to potential employers and is a positive in most people's eyes when evaluating your potential. 

**2)** An introduction to writing GUI applications. From my experience before I started writing web applications all of my work was in the form of command line utilities. While these are great from a programmers perspective they aren't very useful for people who are scared of opening their terminals. To transition to true GUI development I found using the web as a very useful resource. It provides both the easy to use HTML/CSS combination along with the portability of a browser. Anywhere you can run a browser you can run your application.

# The Process

The process is actually very simple. Here I'll notate the steps and a little explenation where needed. 

I'll preface by saying that these steps will be done on Ubuntu Server 18.10. In most cases these commands can be modified to use your distrobution's package manager instead of apt with no issue. 

## Ensure you have the latest packages and updates

To do this you can run two commands on Ubuntu

```shell
$ sudo apt-get update

$ sudo apt-get upgrade -y
```

If you're like me and don't want to hit enter twice then you can combine these commands to `sudo apt-get update && sudo apt-get upgrade -y`

## Install Apache2

To install apache and get our website online we can simply run the command `sudo apt-get install apache2`

After we do this we can use a browser to go to the IP of our machine and we should be greeted with the default Apache page. 

![](/static/apache.png)

Until you get done with port forwarding (which this post won't discuss) then this will only be accessible while you're using a computer on the same network as the server running the website.

## Install PHP

PHP requires a few packages but it can still be done simply with one line

`sudo apt-get install php libapache2-mod-php php-mysql`

This is broken down into three packages installs

1.    **php** - This is how your server will understand PHP code you write
2.    **libapache2-mod-php** - This will enable apache2 to work with PHP files that you'll be writing
3.    **php-mysql** - If you use php to interact with your database then this will enable that behavior (we won't be using this here but it saves you a step later)

### Why use PHP?

So at this point you might have noticed that we're installing a second language for our website since browsers by default support [javascript](https://en.wikipedia.org/wiki/JavaScript) so why do we need PHP? The short answer is that javascript runs on the client side while PHP runs on the server side. 

I would go into more detail about the difference between client side and server side but it just so happens that [much smarter people have written about this topic already](https://www.sqa.org.uk/e-learning/ClientSide01CD/page_18.htm)

## Install MySQL

This is as simple as running `sudo apt-get install mysql-server`

after this from your server's command line you can run `mysql` to get an interactive MySQL shell. If you connect to this with PHP you can specify the IP as `localhost` or `127.0.0.1`

# Celebrate

That's it. Your web server is up and running with all the bells and whistles you could hope for.

![](/static/sl.gif)

# Disclaimers:

If you open your website to the internet by [port forwarding](https://en.wikipedia.org/wiki/Port_forwarding) then anyone on the internet will be able to connect to your server via port 80. For 90% of cases I would say this isn't something to worry about but it is something to keep in mind when working with the internet.

## About the Presenter
* **Name:** Michael Riesberg-Timmer
* **Email:** Michael82097@gmail.com
* **Class Status:** Senior
* **Major:** Computer Science (BA)
* **Years on Linux:** 5