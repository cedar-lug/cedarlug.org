from flask import Flask, render_template, Markup
import markdown
from pygments import highlight
from flask_flatpages import FlatPages, pygments_style_defs

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/officers')
def officers():
    return render_template('officers.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/upcoming')
def upcoming():
    return render_template('upcoming.html')


# The archive page takes an optional parameter to specify a post
# If no post is specified it defaults to None which returns the 
# list of all the archived posts in the Cedar LUG history.
@app.route('/archive', defaults={'post': None})
@app.route('/archive/<post>')
def archives(post):
    if(post):
        # We want to render a specific post
        with open('/var/www/cedarlug.org/static/markdown/' + post + '.md') as content:
            output = content.read()
            output = Markup(
                markdown.markdown(
                    output,
                    extensions = ['fenced_code', 'codehilite']
                    )
                )
            return render_template('archive_post.html', post_text = output)
    else:
        # Return the list of archived posts
        return render_template('archive.html')

@app.route('/mail')
def mailing():
    return render_template('mailing.html')

# This handles any "page not found" errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.route('/pygments.css')
def pygments_css():
    return pygments_style_defs('native'), 200, {'Content-Type': 'text/css'}
